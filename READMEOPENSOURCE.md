[
    {
        "Name": "StatusBarUtil",
        "License": "Apache License Version 2.0",
        "License File": "LICENSE.txt",
        "Version Number": "1.7.5",
        "Upstream URL": "https://github.com/Ye-Miao/StatusBarUtil",
        "Description": "OpenHarmony 沉浸式状态栏，支持状态栏渐变色，纯色， 全屏，亮光、暗色模式，"
    }
]