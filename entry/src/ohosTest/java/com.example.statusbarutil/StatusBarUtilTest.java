package com.example.statusbarutil;

import com.example.library.StatusBarUtil;
import com.example.statusbarutil.ability.MainAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.service.WindowManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;


public class StatusBarUtilTest {

    private MainAbility ability;
    private AbilitySlice slice;
    private  Method cipherColor;

    @Before
    public void setUp() throws Exception {
        Intent intent=new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.example.statusbarutil")
                .withAbilityName("com.example.statusbarutil.ability.MainAbility")
                .build();
        intent.setOperation(operation);
        ability = (MainAbility) AbilityDelegatorRegistry.getAbilityDelegator().startAbilitySync(intent).get();
        slice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        cipherColor = StatusBarUtil.class.getDeclaredMethod("cipherColor", int.class, int.class);
        cipherColor.setAccessible(true);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void setDarkMode() {
        int i = WindowManager.getInstance().getTopWindow().get().getStatusBarVisibility();
        StatusBarUtil.setDarkMode(slice);
        int i1 = WindowManager.getInstance().getTopWindow().get().getStatusBarVisibility();
        Assert.assertNotEquals(i,i1);
    }

    @Test
    public void setLightMode() {
        int i = WindowManager.getInstance().getTopWindow().get().getStatusBarVisibility();
        StatusBarUtil.setLightMode(slice);
        int i1 = WindowManager.getInstance().getTopWindow().get().getStatusBarVisibility();
        Assert.assertNotEquals(i,i1);
    }

   @Test
    public void cipherColor() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Random random = new Random();
        int  mColor = 0xff000000 | random.nextInt(0xffffff);
        int  mAlpha = 0;
        int invoke = (int) cipherColor.invoke(null, mColor, mAlpha);
        Assert.assertTrue(invoke == mColor);
    }

    @Test
    public void testCipherColor() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Random random = new Random();
        int  mColor = 0xff000000 | random.nextInt(0xffffff);
        int  mAlpha = 10;
        int invoke = (int) cipherColor.invoke(null, mColor, mAlpha);
        Assert.assertTrue(invoke != mColor);
    }

}