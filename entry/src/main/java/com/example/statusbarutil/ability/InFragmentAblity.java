package com.example.statusbarutil.ability;

import com.example.statusbarutil.slice.InFragmentAbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class InFragmentAblity extends FractionAbility {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(InFragmentAbilitySlice.class.getName());
    }
}
