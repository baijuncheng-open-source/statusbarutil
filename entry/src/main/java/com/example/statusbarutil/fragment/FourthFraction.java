package com.example.statusbarutil.fragment;

import com.example.statusbarutil.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ComponentContainer;

public class FourthFraction extends Fraction {
    private DependentLayout dependentlayout ;
    private Component component;
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = scatter.parse(ResourceTable.Layout_fourth_fragment, container, false);
        return component;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        dependentlayout = (DependentLayout) component.findComponentById(ResourceTable.Id_titleView);
        dependentlayout.setClickedListener(new Component.ClickedListener() {
               @Override
               public void onClick(Component component) {
                 getFractionAbility().terminateAbility();
               }
           });
    }

    public static FourthFraction newInstance() {
        return new FourthFraction();
    }

}
