package com.example.statusbarutil.fragment;


import com.example.statusbarutil.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;

/**
 * @author：created by apple on 2019-11-26
 * email: 15622113269@163.com
 * Desc:
 */
public class ThirdFraction extends Fraction {
    private Component component;
    private DependentLayout dependentlayout;
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = scatter.parse(ResourceTable.Layout_third_fragment, container, false);
        return component;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    public static ThirdFraction newInstance() {
        return new ThirdFraction();
    }
}
