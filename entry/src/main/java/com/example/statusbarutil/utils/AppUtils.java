package com.example.statusbarutil.utils;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.*;
import ohos.system.DeviceInfo;
import ohos.system.version.SystemVersion;
import java.io.IOException;
import java.util.Locale;

public class AppUtils {

    private AppUtils() {

    }

    private static Context mContext;
    private static Thread mUiThread;
    private static int i;

    private static EventHandler sHandler = new EventHandler(EventRunner.getMainEventRunner());

    public static void init(Context context) { //在Application中初始化
        mContext = context;
        mUiThread = Thread.currentThread();
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static RawFileEntry getAssets() {
        return getResource().getRawFileEntry("resources/base/rawfile");
    }

    public static ResourceManager getResource() {
        return mContext.getResourceManager();
    }

    public static Element getDrawable(int rseId) {
        ShapeElement shapeElement = new ShapeElement(mContext,rseId);
        return shapeElement;
    }

    public static int getColor(int resId) {
        try {
         i = getResource().getElement(resId).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return i;
    }

    public static boolean isUIThread() {
        return Thread.currentThread() == mUiThread;
    }

    public static void runOnUI(Runnable r) {
        sHandler.postTask(r);
    }

    public static void runOnUIDelayed(Runnable r, long delayMills) {
        sHandler.postTimingTask(r, delayMills);
    }


    /**
     * 获取当前手机系统语言。
     *
     * @return 返回当前系统语言。例如：当前设置的是“中文-中国”，则返回“zh-CN”
     */
    public static String getSystemLanguage() {
        return Locale.getDefault().getLanguage();
    }

    /**
     * 获取当前手机系统版本号
     *
     * @return 系统版本号
     */
    public static String getSystemVersion() {
        return SystemVersion.getVersion();
    }

    /**
     * 获取手机型号
     *
     * @return 手机型号
     */
    public static String getSystemModel() {
        return DeviceInfo.getModel();
    }


    /**
     * 获取手机厂商
     *
     * @return 手机厂商
     */
    public static String getDeviceBrand() {
        return DeviceInfo.getDeviceType();
    }

}
