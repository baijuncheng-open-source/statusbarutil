package com.example.statusbarutil.slice;

import com.example.library.StatusBarUtil;
import com.example.statusbarutil.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import java.io.IOException;

public class TransparentAbilitySlice extends AbilitySlice {
    private boolean isChanged;
    private Resource bgResource1,bgResource2 ;
    private PixelMapElement pixBg1,pixBg2;
    private DependentLayout relativeLayout;
    private Button btn;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_set_transparent);
        StatusBarUtil.setTransparentForWindow(this);

         btn = (Button) findComponentById(ResourceTable.Id_button1);
         relativeLayout = (DependentLayout) findComponentById(ResourceTable.Id_container);
        try {
            //获取Media文件夹中的图片资源
            bgResource1 =getResourceManager().getResource(ResourceTable.Media_photo5);
            bgResource2 =getResourceManager().getResource(ResourceTable.Media_photo6);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        //根据资源生成PixelMapElement实例
        pixBg1=new PixelMapElement(bgResource1);
        pixBg2=new PixelMapElement(bgResource2);
        relativeLayout.setBackground(pixBg2);
        btn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                isChanged = !isChanged;
                if (isChanged) {
                    relativeLayout.setBackground(pixBg1);
                } else {
                    relativeLayout.setBackground(pixBg2);
                }
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
