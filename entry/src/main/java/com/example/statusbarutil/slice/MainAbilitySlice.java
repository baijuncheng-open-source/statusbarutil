package com.example.statusbarutil.slice;

import com.example.library.StatusBarUtil;
import com.example.statusbarutil.ResourceTable;
import com.example.statusbarutil.utils.AppUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.service.WindowManager;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Button btn = (Button) findComponentById(ResourceTable.Id_btn);
        Button btn1 = (Button) findComponentById(ResourceTable.Id_btn1);
        Button btn2 = (Button) findComponentById(ResourceTable.Id_btn2);
        Button btn3 = (Button) findComponentById(ResourceTable.Id_btn3);
        Button btn4 = (Button) findComponentById(ResourceTable.Id_btn4);
        Button btn5 = (Button) findComponentById(ResourceTable.Id_btn5);

        Text mVersionText = (Text) findComponentById(ResourceTable.Id_version_text);

        /*
         *  设置状态栏颜色
         */
        btn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present( new ColorAbilitySlice(),new Intent());
            }
        });

        /*
         *  设置透明
         */
        btn1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new TransparentAbilitySlice(),new Intent());
            }
        });

        /*
         *  设置状态栏渐变
         */
        btn2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new GradientAbilitySlice(),new Intent());
            }
        });

        /*
         * 在Fraction中使用
         */
        btn3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.example.statusbarutil")
                        .withAbilityName("com.example.statusbarutil.ability.InFragmentAblity")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });

        btn4.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                StatusBarUtil.setLightMode(MainAbilitySlice.this);
            }
        });

        btn5.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                StatusBarUtil.setDarkMode(MainAbilitySlice.this);
            }
        });

        mVersionText.setText(
                "手机厂商" + AppUtils.getDeviceBrand() + "\n" +
                        "手机型号" + AppUtils.getSystemModel() + "\n" +
                        "系统版本号" + AppUtils.getSystemVersion()
        );

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
    }
}
