package com.example.statusbarutil.slice;

import com.example.library.StatusBarUtil;
import com.example.statusbarutil.ResourceTable;
import com.example.statusbarutil.fragment.FirstFraction;
import com.example.statusbarutil.fragment.FourthFraction;
import com.example.statusbarutil.fragment.SecondFraction;
import com.example.statusbarutil.fragment.ThirdFraction;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class InFragmentAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private  FractionScheduler fractionscheduler;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_use_in_fragment);
        StatusBarUtil.setTransparentForWindow(this);
        Button bt1 = (Button) findComponentById(ResourceTable.Id_one);
        Button bt2 = (Button) findComponentById(ResourceTable.Id_two);
        Button bt3 = (Button) findComponentById(ResourceTable.Id_three);
        Button bt4 = (Button) findComponentById(ResourceTable.Id_four);
        bt1.setClickedListener(this);
        bt2.setClickedListener(this);
        bt3.setClickedListener(this);
        bt4.setClickedListener(this);
        ((FractionAbility)getAbility()).getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_fl_content, new FirstFraction())
                .submit();
    }

    @Override
    public void onClick(Component component) {
        fractionscheduler =((FractionAbility) getAbility()).getFractionManager().startFractionScheduler();
        switch (component.getId()){
            case ResourceTable.Id_one:
                fractionscheduler.replace(ResourceTable.Id_fl_content,new FirstFraction()).submit();
                break;
            case ResourceTable.Id_two:
                fractionscheduler.replace(ResourceTable.Id_fl_content,new SecondFraction()).submit();
                break;
            case ResourceTable.Id_three:
                fractionscheduler.replace(ResourceTable.Id_fl_content,new ThirdFraction()).submit();
                break;
            case ResourceTable.Id_four:
                fractionscheduler.replace(ResourceTable.Id_fl_content,new FourthFraction()).submit();
                break;
        }
    }
}
