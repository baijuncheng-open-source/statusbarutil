package com.example.statusbarutil.slice;

import com.example.library.StatusBarUtil;
import com.example.statusbarutil.ResourceTable;
import com.example.statusbarutil.utils.AppUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Slider;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.service.WindowManager;
import java.util.Random;

public class ColorAbilitySlice extends AbilitySlice {
    private int mColor;
    private int mAlpha = 0;
    private DependentLayout dependentLayout;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        setUIContent(ResourceTable.Layout_set_color);
        StatusBarUtil.setColor(this,AppUtils.getColor(ResourceTable.Color_colorPrimary) ,mAlpha);
        StatusBarUtil.setLightMode(ColorAbilitySlice.this);

        dependentLayout = (DependentLayout) findComponentById(ResourceTable.Id_titleBar1);
        Slider mSlider = (Slider) findComponentById(ResourceTable.Id_mSlider);
        Button mChangeColor = (Button) findComponentById(ResourceTable.Id_change_color);
        Text mVersionText = (Text) findComponentById(ResourceTable.Id_version_text);
        Text mAlphaText = (Text) findComponentById(ResourceTable.Id_alpha_text);

        mChangeColor.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                changeColor();
            }
        });

        mAlphaText.setText(String.valueOf(mAlpha));
        mSlider.setProgressValue(0);
        mSlider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mAlpha = i;
                StatusBarUtil.setColor(ColorAbilitySlice.this, mColor, mAlpha);
                mAlphaText.setText(String.valueOf(mAlpha));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mVersionText.setText(
                "手机厂商" + AppUtils.getDeviceBrand() + "\n" +
                        "手机型号" + AppUtils.getSystemModel() + "\n" +
                        "Ohos系统版本号" + AppUtils.getSystemVersion()
        );
    }

    public void changeColor() {
        Random random = new Random();
        mColor = 0xff000000 | random.nextInt(0xffffff);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(mColor));
        dependentLayout.setBackground(shapeElement);
        StatusBarUtil.setColor(this, mColor, mAlpha);

    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        getWindow().clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
    }
}
