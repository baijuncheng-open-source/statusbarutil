package com.example.statusbarutil.slice;

import com.example.library.StatusBarUtil;
import com.example.statusbarutil.ResourceTable;
import com.example.statusbarutil.annotation.SuppressLint;
import com.example.statusbarutil.utils.AppUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.service.WindowManager;
import java.util.Random;

public class GradientAbilitySlice extends AbilitySlice {

    private DependentLayout dependentlayout;
    private int mColor;
    private int mAlpha = 0;
    private RgbColor[] rgbColors;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        setUIContent(ResourceTable.Layout_gradient_color);
        mColor = AppUtils.getColor(ResourceTable.Color_colorPrimary);
        StatusBarUtil.setColor(GradientAbilitySlice.this,mColor,mAlpha);
        Button mSetGradient = (Button) findComponentById(ResourceTable.Id_set_gradient);
        Text mVersionText = (Text) findComponentById(ResourceTable.Id_version_text);
        dependentlayout = (DependentLayout) findComponentById(ResourceTable.Id_titleView);

        mSetGradient.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                changeColor();
            }
        });

        mVersionText.setText(
                "手机厂商" + AppUtils.getDeviceBrand() + "\n" +
                        "手机型号" + AppUtils.getSystemModel() + "\n" +
                        "系统版本号" + AppUtils.getSystemVersion()
        );
    }

    @SuppressLint("NewApi")
    private void changeColor() {
        Random random = new Random();
        rgbColors = new RgbColor[]{new RgbColor(random.nextInt(), random.nextInt(), random.nextInt()),
                new RgbColor(random.nextInt(), random.nextInt(), random.nextInt())};

        //设置背景色和渐变(从下端角到上端角)
        ShapeElement gradient_element = new ShapeElement();
        gradient_element.setShape(ShapeElement.RECTANGLE);
        gradient_element.setRgbColors(rgbColors);
        gradient_element.setGradientOrientation(ShapeElement.Orientation.BOTTOM_END_TO_TOP_START);
        dependentlayout.setBackground(gradient_element);
    }
}
