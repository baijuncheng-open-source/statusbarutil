package com.example.statusbarutil;

import com.example.statusbarutil.utils.AppUtils;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {

    @Override
    public void onInitialize(){
        super.onInitialize();
        AppUtils.init(this);
    }
}
