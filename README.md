### 简介
>Statusbarutil是一个实现沉浸式状态栏，支持状态栏渐变色，纯色， 全屏，亮光、暗色模式等功能的库

Demo

![](images/StatusBarUtil.gif)

###  **使用**

直接导入源码使用

1.下载源码后，将源码中的library模块copy到自己项目中，与entry同级。
       将library作为一个模块，参照源码demo，与自己的其他模块相关联。

2.搭配如har包使用
   将项目运行后生成的har包，放入到自己的lib中，添加完依赖后，使用方法导入源码中调用即可。

远程依赖
```
dependencies {
   implementation ‘com.gitee.baijuncheng-open-source:StatusBarUtil:1.0.0’
}
```
	
二、使用方法

       1.设置纯色状态栏
         StatusBarUtil.setColor(this, mColor); 
	
        2.设置透明状态栏
         StatusBarUtil.setTransparentForWindow(this);

       3.设置亮色模式
           StatusBarUtil.setLightMode(this);

### License
Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.




