package com.example.library;

import com.example.statusbarutil.annotation.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import java.lang.ref.WeakReference;

/**
 * Desc: 状态栏工具类
 */
public class StatusBarUtil {

    private static final int DEFAULT_ALPHA = 0;
    /**
     * 设置状态栏颜色（自定义颜色)
     * @param abilitySlice 目标ability
     * @param color    状态栏颜色值
     */
    public static void setColor(@NonNull AbilitySlice abilitySlice, @ColorInt int color) {
        WeakReference<AbilitySlice> abilitySliceWeakReference = new WeakReference<>(abilitySlice);
        setColor(abilitySliceWeakReference.get(), color, DEFAULT_ALPHA);
    }

    /**
     * 设置纯色状态栏（自定义颜色，alpha）
     */
    public static void setColor(@NonNull AbilitySlice abilitySlice, @ColorInt int color, @IntRange(from = 0, to = 255) int alpha) {
        WeakReference<AbilitySlice> abilitySliceWeakReference = new WeakReference<>(abilitySlice);
        Window window = abilitySliceWeakReference.get().getWindow();
        window.setStatusBarColor(cipherColor(color,alpha));
    }

    /**
     * 设置透明状态栏
     */
    public static void setTransparentForWindow(@NonNull AbilitySlice abilitySlice) {
        WeakReference<AbilitySlice> abilitySliceWeakReference = new WeakReference<>(abilitySlice);
        Window window = abilitySliceWeakReference.get().getWindow();
        window.addFlags(WindowManager.LayoutConfig.MARK_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
        window.setStatusBarColor(Color.TRANSPARENT.getValue());
    }

    /**
     * 设置状态栏darkMode,字体颜色及icon变黑
     */
    public static void setDarkMode(@NonNull AbilitySlice abilitySlice) {
        WeakReference<AbilitySlice> abilitySliceWeakReference = new WeakReference<>(abilitySlice);
        abilitySliceWeakReference.get().getWindow().setStatusBarVisibility(2048);
    }

    /**
     * 设置状态栏darkMode,字体颜色及icon变亮
     * 1000 0000 0100
     */
    public static void setLightMode(@NonNull AbilitySlice abilitySlice) {
        WeakReference<AbilitySlice> abilitySliceWeakReference = new WeakReference<>(abilitySlice);
        abilitySliceWeakReference.get().getWindow().setStatusBarVisibility(0);
    }

    /**
     * 计算alpha色值
     * @param color 状态栏颜色值
     * @param alpha 状态栏透明度
     */
    private static int cipherColor(@ColorInt int color, int alpha) {
        if (alpha == 0) {
            return color;
        }
        float a = 1 - alpha / 255f;
        int red = color >> 16 & 0xff;
        int green = color >> 8 & 0xff;
        int blue = color & 0xff;
        red = (int) (red * a + 0.5);
        green = (int) (green * a + 0.5);
        blue = (int) (blue * a + 0.5);
        return 0xff << 24 | red << 16 | green << 8 | blue;
    }
}